#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os

from subprocess import run

root_folder = os.path.realpath(os.path.abspath(os.path.join(
    os.path.normpath(os.path.dirname(__file__)))))


class ANSIColors():

    def ERROR(self, string):
        return "\033[38;5;166;1m" + str(string) + "\033[0m"

    def INFO(self, string):
        return "\033[1m" + str(string) + "\033[0m"

    def WARNING(self, string):
        return "\033[38;5;220;1m" + str(string) + "\033[0m"

    def SUCCESS(self, string):
        return "\033[38;5;77;1m" + str(string) + "\033[0m"

    def PURPLE(self, string):
        return "\033[38;5;164;1m" + str(string) + "\033[0m"


Ansi = ANSIColors()


def get_environment(set_vars={}, unset_vars=[]):
    env = {}
    env.update(os.environ)

    if set_vars:
        env.update(set_vars)

    if unset_vars:
        for var in unset_vars:
            del env[var]

    return env


def can_exec(path):
    return os.path.isfile(path) and os.access(path, os.X_OK)


def which(cmd):
    for path in find_executables(cmd):
        return path

    return None


def find_executables(executable):
    env = get_environment()

    for base in env.get("PATH", "").split(os.pathsep):
        path = os.path.join(os.path.expanduser(base), executable)

        if can_exec(path):
            yield path

    return None


if __name__ == "__main__":
    if which("sass"):
        sass_cmd = ["sass"]
        sass_cmd_arg_files = "%s:%s"
        sass_cmd_arg_2 = ["--no-source-map"]
    elif which("sassc"):
        sass_cmd = ["sassc"]
        sass_cmd_arg_files = "%s %s"
        sass_cmd_arg_2 = []
    else:
        raise SystemExit("Missing sass command. Read the README.")

    dist_folder = os.path.join(root_folder, "dist")
    sass_file = os.path.join(root_folder, "flatly_bootstrap_theme.scss")
    css_file_expanded = os.path.join(dist_folder, "flatly_bootstrap_theme.css")
    css_file_compressed = os.path.join(dist_folder, "flatly_bootstrap_theme.min.css")

    os.makedirs(dist_folder, exist_ok=True)

    print(Ansi.INFO("Building expanded stylesheet..."))
    final_cmd = " ".join(sass_cmd + [sass_cmd_arg_files % (sass_file, css_file_expanded)
                                     ] + ["--style=expanded"] + sass_cmd_arg_2)
    print(Ansi.INFO("Executed command:"))
    print(final_cmd)
    run(final_cmd, shell=True)
    print()

    print(Ansi.INFO("Building compressed stylesheet..."))
    final_cmd = " ".join(sass_cmd + [sass_cmd_arg_files % (sass_file, css_file_compressed)
                                     ] + ["--style=compressed"] + sass_cmd_arg_2)
    print(Ansi.INFO("Executed command:"))
    print(final_cmd)
    run(final_cmd, shell=True)
    print()

    print(Ansi.INFO("Autoprefixing..."))
    final_cmd = "npx postcss dist/*.css --dir dist/"
    print(Ansi.INFO("Executed command:"))
    print(final_cmd)

    try:
        run(final_cmd, shell=True)
    except Exception:
        raise SystemExit(
            "Either Node.js or the required node modules are missing. Read the README.")
